output "codepipeline_name" {
  value = aws_codepipeline.codepipeline.name
}

output "artifacts_location" {
  value = aws_s3_bucket.codepipeline_bucket.bucket
}