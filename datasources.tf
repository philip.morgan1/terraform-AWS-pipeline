data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "lambda_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "AWSLambdaTrustPolicy" {
  statement {
    actions = [
      "logs:DisassociateKmsKey",
      "logs:DeleteSubscriptionFilter",
      "logs:UntagLogGroup",
      "logs:DeleteLogGroup",
      "logs:StartQuery",
      "logs:CreateLogGroup",
      "logs:DeleteLogStream",
      "logs:PutLogEvents",
      "logs:CreateExportTask",
      "logs:PutMetricFilter",
      "logs:CreateLogStream",
      "logs:DeleteMetricFilter",
      "logs:TagLogGroup",
      "logs:DeleteRetentionPolicy",
      "logs:GetLogEvents",
      "logs:AssociateKmsKey",
      "logs:FilterLogEvents",
      "logs:PutSubscriptionFilter",
      "logs:PutRetentionPolicy",
      "logs:GetLogGroupFields",
      "logs:GetLogRecord",
      "ses:VerifyEmailIdentity",
      "ses:GetIdentityPolicies",
      "ses:GetSendQuota",
      "ses:DescribeConfigurationSet",
      "ses:GetIdentityMailFromDomainAttributes",
      "ses:VerifyDomainDkim",
      "logs:GetLogDelivery",
      "ses:VerifyDomainIdentity",
      "ses:SendEmail",
      "logs:DeleteResourcePolicy",
      "ses:GetIdentityDkimAttributes",
      "ses:DescribeReceiptRuleSet",
      "logs:CancelExportTask",
      "logs:DeleteLogDelivery",
      "ses:GetTemplate",
      "logs:PutDestination",
      "ses:VerifyEmailAddress",
      "ses:GetCustomVerificationEmailTemplate",
      "logs:PutDestinationPolicy",
      "ses:GetSendStatistics",
      "ses:SendRawEmail",
      "logs:StopQuery",
      "logs:TestMetricFilter",
      "ses:GetIdentityVerificationAttributes",
      "logs:DeleteDestination",
      "ses:GetIdentityNotificationAttributes",
      "ses:DescribeReceiptRule",
      "logs:CreateLogDelivery",
      "ses:DescribeActiveReceiptRuleSet",
      "ses:GetAccountSendingEnabled",
      "logs:PutResourcePolicy",
      "logs:GetQueryResults",
      "logs:UpdateLogDelivery",
      "logs:*"
    ]
    resources = ["*"]
    effect    = "Allow"
  }
}

data "archive_file" "lambdaCode" {
  count       = var.factory_pipeline ? 1 : 0
  type        = "zip"
  source_file = "${path.module}/lambda_function.py"
  output_path = "lambda_function.zip"
}