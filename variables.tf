## Required

variable "pipeline_name" {
  description = "The name of the AWS CodePipeline"
  type        = string
}

variable "codestar_connection_guid" {
  description = "The CodeStar connection GUID"
  type        = string
}

variable "bitbucket_workspace" {
  description = "The name of the Bitbucket workspace in which the repository lives"
  type        = string
}

variable "bitbucket_repository_name" {
  description = "The name of the repository which will trigger the pipeline"
  type        = string
}

variable "pipeline_stages" {
  description = "The CodePipeline stages declared as JSON"
  type        = string
}

variable "codebuild_policy" {
  description = "The CodeBuild policies containing the permissions for the CodeBuild steps of your CodePipeline"
  type        = list
}

## Optional

variable "bitbucket_repository_branch_name" {
  description = "The name of the branch which will trigger the pipeline"
  default     = "master"
  type        = string
}

variable "privileged_mode" {
  description = "Whether privileged mode in which the codebuild project is on or off"
  default     = false
}

variable "build_image" {
  description = "The Docker image to use for this build project."
  default     = "aws/codebuild/standard:4.0"
  type        = string
}

variable "container_type" {
  description = "The type of build environment to use for related builds. Available values are: LINUX_CONTAINER, LINUX_GPU_CONTAINER, WINDOWS_CONTAINER or ARM_CONTAINER."
  default     = "LINUX_CONTAINER"
  type        = string
}

variable "custom_tags" {
  description = "A map of tags that will be assigned to each resource created by the module"
  default     = null
  type        = map
}

variable "notification_email" {
  description = "The email you would like notifications sending to"
  type        = string
  default     = ""
}

variable "factory_pipeline" {
  description = "This should be a 1 if the pipeline being created is to be within the pipeline factory"
  type        = bool
  default     = false
}

variable "lambda_region" {
  description = "The region of the lambda function"
  default     = "eu-west-1"
}