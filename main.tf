## Locals

locals {
  pipeline_short_name = substr(var.pipeline_name, 0, 44)
  pipeline_stages     = jsondecode(var.pipeline_stages)
  build_projects = [
    for build in local.pipeline_stages.stages :
    build if build.actionType == "Build"
  ]
  codebuild_stages = [
    for build in local.pipeline_stages.stages :
    "${var.pipeline_name}-${build.stageName}" if build.actionType == "Build"
  ]
}

## CodePipeline

resource "aws_codepipeline" "codepipeline" {
  name     = var.pipeline_name
  role_arn = aws_iam_role.codepipeline_role.arn

  artifact_store {
    location = aws_s3_bucket.codepipeline_bucket.bucket
    type     = "S3"
  }

  stage {
    name = "Source"

    action {
      name             = var.bitbucket_repository_name
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeStarSourceConnection"
      version          = "1"
      output_artifacts = ["source"]

      configuration = {
        ConnectionArn        = "arn:aws:codestar-connections:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:connection/${var.codestar_connection_guid}"
        FullRepositoryId     = "${var.bitbucket_workspace}/${var.bitbucket_repository_name}"
        BranchName           = var.bitbucket_repository_branch_name
        OutputArtifactFormat = "CODE_ZIP"
      }
    }
  }

  dynamic "stage" {
    for_each = [for s in local.pipeline_stages.stages : {
      stage_name  = s.stageName
      action_type = s.actionType
      artifacts   = lookup(s, "artifacts", null)
    }]

    content {
      name = stage.value.stage_name

      action {
        name             = var.bitbucket_repository_name
        category         = stage.value.action_type
        owner            = "AWS"
        provider         = stage.value.action_type == "Build" ? "CodeBuild" : "Manual"
        input_artifacts  = stage.value.artifacts != null ? lookup(stage.value.artifacts, "input", null) : null
        output_artifacts = stage.value.artifacts != null ? lookup(stage.value.artifacts, "output", null) : null
        version          = "1"
        run_order        = stage.key + 1
        configuration = {
          ProjectName = stage.value.action_type == "Build" ? "${var.pipeline_name}-${stage.value.stage_name}" : null
        }
      }
    }
  }
  depends_on = [aws_iam_role_policy.codepipeline_policy]
  tags       = var.custom_tags
}

## CodeBuild

resource "aws_codebuild_project" "code_build_project" {
  count          = length(local.build_projects)
  name           = "${var.pipeline_name}-${local.build_projects[count.index].stageName}"
  description    = lookup(local.build_projects[count.index], "description", "")
  build_timeout  = lookup(local.build_projects[count.index], "buildTimeout", "60")
  queued_timeout = lookup(local.build_projects[count.index], "queueTimeout", "480")

  service_role = aws_iam_role.codebuild_service_role.arn

  artifacts {
    type = "CODEPIPELINE"
  }

  environment {
    compute_type    = "BUILD_GENERAL1_SMALL"
    privileged_mode = var.privileged_mode
    image           = var.build_image
    type            = var.container_type

    dynamic "environment_variable" {
      for_each = [for e in local.build_projects[count.index].environmentVariables : {
        name  = e.name
        value = e.value
      }]

      content {
        name  = environment_variable.value.name
        value = environment_variable.value.value
      }
    }
  }

  source {
    type      = "CODEPIPELINE"
    buildspec = lookup(local.build_projects[count.index], "buildspec", "buildspec.yml")
  }

  tags = var.custom_tags
}

## S3

resource "random_id" "s3_bucket_name" {
  byte_length = 2
  prefix      = "${local.pipeline_short_name}-codepipeline-"
  keepers = {
    pipeline_name = var.pipeline_name
  }
}

resource "aws_s3_bucket" "codepipeline_bucket" {
  bucket = random_id.s3_bucket_name.hex
  acl    = "private"
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  force_destroy = true
  tags          = var.custom_tags
  #checkov:skip=CKV_AWS_52:Ensure S3 bucket has MFA delete enabled - Not required as the CodePipeline artefacts are disposable
  #checkov:skip=CKV_AWS_18:Ensure the S3 bucket has access logging enabled - Not required as the bucket is only used for CodePipeline artefacts
}

## IAM

resource "aws_iam_role" "codepipeline_role" {
  name = "${local.pipeline_short_name}-codepipeline-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codepipeline.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
  tags               = var.custom_tags
}

resource "aws_iam_role_policy" "codepipeline_policy" {
  name = "codepipeline-policy"
  role = aws_iam_role.codepipeline_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect":"Allow",
      "Action": [
        "s3:*"
      ],
      "Resource": [
        "${aws_s3_bucket.codepipeline_bucket.arn}",
        "${aws_s3_bucket.codepipeline_bucket.arn}/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "codebuild:BatchGetBuilds",
        "codebuild:StartBuild"
      ],
      "Resource": [
        "arn:aws:codebuild:*:${data.aws_caller_identity.current.account_id}:*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "codestar-connections:UseConnection"
      ],
      "Resource": [
        "arn:aws:codestar-connections:*:${data.aws_caller_identity.current.account_id}:connection/*"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_role" "codebuild_service_role" {
  name                  = "${local.pipeline_short_name}-codebuild-role"
  permissions_boundary  = aws_iam_policy.codebuild_boundary_policy.id
  force_detach_policies = true
  assume_role_policy    = <<Assume
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codebuild.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
Assume
  tags                  = var.custom_tags
}

resource "aws_iam_role_policy" "codebuild_policy" {
  count  = length(var.codebuild_policy)
  role   = aws_iam_role.codebuild_service_role.name
  policy = var.codebuild_policy[count.index]
}

resource "aws_iam_role_policy" "codebuild_default_policy" {
  role   = aws_iam_role.codebuild_service_role.name
  policy = <<Policy
{
    "Version": "2012-10-17",
    "Statement": [{
        "Effect": "Allow",
        "Action": [
            "s3:ListBucket",
            "s3:PutObject",
            "s3:GetObject",
            "s3:GetObjectVersion",
            "s3:CreateBucket",
            "s3:PutBucketEncryption"
        ],
        "Resource": [
          "arn:aws:s3:::*"
        ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "dynamodb:PutItem",
        "dynamodb:GetItem",
        "dynamodb:DeleteItem",
        "dynamodb:CreateTable"
      ],
      "Resource": [
          "arn:aws:dynamodb:*:${data.aws_caller_identity.current.account_id}:*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": [
          "arn:aws:logs:*:${data.aws_caller_identity.current.account_id}:log-group:*"
      ]
    }]
}
Policy
}

resource "aws_iam_policy" "codebuild_boundary_policy" {
  name = "${var.pipeline_name}-codebuild-policy-boundary"

  policy = <<Boundary
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "*",
      "Resource": "*"
    },
    {
      "Sid": "CodeBuildDeny",
      "Effect": "Deny",
      "Action": [
        "trail:*",
        "guardduty:*"
      ],
      "Resource": "*"
    }
  ]
}
Boundary
}

## SNS

resource "aws_sns_topic" "sns_codepipeline_notification" {
  name = "${var.pipeline_name}-codepipeline-notifications"
  tags = var.custom_tags
}

resource "aws_sns_topic_policy" "sns_codepipeline_notification_policy" {
  arn    = aws_sns_topic.sns_codepipeline_notification.arn
  policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": [
        "SNS:GetTopicAttributes",
        "SNS:SetTopicAttributes",
        "SNS:AddPermission",
        "SNS:RemovePermission",
        "SNS:DeleteTopic",
        "SNS:Subscribe",
        "SNS:ListSubscriptionsByTopic",
        "SNS:Publish",
        "SNS:Receive"
      ],
      "Resource": "${aws_sns_topic.sns_codepipeline_notification.arn}"
    }
  ]
}
EOF
}

## CloudWatch

resource "aws_cloudwatch_event_rule" "codebuild_cloudwatch_event_rule" {
  name        = "${local.pipeline_short_name}-codebuild"
  description = "Capture CodeBuild failure events"

  event_pattern = <<EOF
{
  "source": [ 
    "aws.codebuild"
  ], 
  "detail-type": [
    "CodeBuild Build State Change"
  ],
  "detail": {
    "build-status": [
      "FAILED"
    ],
    "project-name": ${jsonencode(local.codebuild_stages)}
  }  
}
EOF
  tags          = var.custom_tags
  depends_on    = [aws_sns_topic.sns_codepipeline_notification]
}

resource "aws_cloudwatch_event_rule" "codepipeline_cloudwatch_event_rule" {
  name        = "${local.pipeline_short_name}-codepipeline"
  description = "Capture CodePipeline Started and Succeeded events"

  event_pattern = <<EOF
{
  "source": [ 
    "aws.codepipeline"
  ], 
  "detail-type": [
    "CodePipeline Pipeline Execution State Change"
  ],
  "detail": {
    "state": [
      "STARTED",
      "SUCCEEDED"
    ],
    "pipeline": [
      ${jsonencode(aws_codepipeline.codepipeline.name)}
    ]
  }  
}
EOF
  tags          = var.custom_tags
  depends_on    = [aws_sns_topic.sns_codepipeline_notification]
}

resource "aws_cloudwatch_event_target" "codebuild_cloudwatch_event_sns_target" {
  rule      = aws_cloudwatch_event_rule.codebuild_cloudwatch_event_rule.name
  target_id = "SendToSNS"
  arn       = aws_sns_topic.sns_codepipeline_notification.arn

  input_transformer {
    input_paths = {
      build_id     = "$.detail.build-id"
      project_name = "$.detail.project-name"
      build_status = "$.detail.build-status"
      log_url      = "$.detail.additional-information.logs.deep-link"
    }
    input_template = "\"CodeBuild project '<project_name>' staus has changed to '<build_status>'. Logs for this build can be found here: <log_url>\""
  }
}

resource "aws_cloudwatch_event_target" "codepipeline_cloudwatch_event_sns_target" {
  rule      = aws_cloudwatch_event_rule.codepipeline_cloudwatch_event_rule.name
  target_id = "SendToSNS"
  arn       = aws_sns_topic.sns_codepipeline_notification.arn

  input_transformer {
    input_paths = {
      pipeline_name  = "$.detail.pipeline"
      pipeline_state = "$.detail.state"
    }
    input_template = "\"Pipeline '<pipeline_name>' status has changed to '<pipeline_state>'.\""
  }
}

resource "aws_cloudwatch_log_subscription_filter" "lambdafunction_logfilter" {
  count = var.factory_pipeline ? 1 : 0

  depends_on      = [aws_lambda_permission.lambda_invoke]
  name            = "lambdafunction_logfilter"
  log_group_name  = "/aws/codebuild/${var.pipeline_name}-Apply"
  filter_pattern  = "\"UPLOAD_ARTIFACTS State: SUCCEEDED\""
  destination_arn = aws_lambda_function.lambda[count.index].arn
}

resource "aws_lambda_permission" "lambda_invoke" {
  count = var.factory_pipeline ? 1 : 0

  depends_on    = [aws_lambda_function.lambda]
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  source_arn    = "arn:aws:logs:${var.lambda_region}:${data.aws_caller_identity.current.account_id}:log-group:/aws/codebuild/${var.pipeline_name}-Apply:*"
  function_name = aws_lambda_function.lambda[count.index].function_name
  principal     = "logs.eu-west-1.amazonaws.com"
}

resource "aws_iam_policy" "lambda_policy" {
  count = var.factory_pipeline ? 1 : 0

  name   = "${var.pipeline_name}-notification_lambda_policy"
  policy = data.aws_iam_policy_document.AWSLambdaTrustPolicy.json
}

resource "aws_iam_role_policy_attachment" "terraform_lambda_assume_policy" {
  count = var.factory_pipeline ? 1 : 0

  role       = aws_iam_role.lambda_role[count.index].name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "terraform_lambda_policy" {
  count = var.factory_pipeline ? 1 : 0

  role       = aws_iam_role.lambda_role[count.index].name
  policy_arn = aws_iam_policy.lambda_policy[count.index].arn
}

resource "aws_iam_role" "lambda_role" {
  count = var.factory_pipeline ? 1 : 0

  name               = "${var.pipeline_name}_function_role"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role_policy.json
}

resource "aws_lambda_function" "lambda" {
  count = var.factory_pipeline ? 1 : 0

  filename         = "lambda_function.zip"
  function_name    = "${var.pipeline_name}-notification-lambda"
  handler          = "lambda_function.lambda_handler"
  role             = aws_iam_role.lambda_role[count.index].arn
  timeout          = 800
  source_code_hash = data.archive_file.lambdaCode[count.index]

  runtime = "python3.7"

  environment {
    variables = {
      emailAddress = var.notification_email,
      logGroupName = "/aws/codebuild/${var.pipeline_name}-Apply"
    }
  }
}