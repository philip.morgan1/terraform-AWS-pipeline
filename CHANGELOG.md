# Change Log

## v 5.0.2

- Refactor the CodePipeline and CodeBuild IAM Policies, and removed permissions not required in line with the principle of least privilege

## Fixes

*N/A*

## v 5.0.1

## Fixes

- Added depends_on to CloudWatch EventRules to ensure the SNS Topic for notifications is removed first on pipeline removal

## v 5.0.0

- ```codebuild_policy``` data type has been changed from string to list, to allow multiple policies to be attached the IAM Role used by CodeBuild

## Fixes

- Restrictions on policy length can be bypassed with multiple policies, facilitating minimum viable permissions

## v 4.0.2

## Fixes

- Fixed a bug with the conditional check on the data source 'archive_file'

## v 4.0.1

## Fixes

- Added a conditional check to the data source 'archive_file'


## v 4.0.0

- Updated the way CodePipeline stages are passed into the module
- Removed the predefined pattern for artifact management, to facilitate different requirements and patterns

## Fixes

- restrictive behaviours around passing artifacts has been removed

## v 3.6.2

### Fixes

- fixed a variable conflict with bool and binary

## v 3.6.2

### Fixes

- fixed an empty variable causing issues

## v 3.6.1

### Fixes

- updated the default permissions for CodeBuild

## v 3.6.0

- added a feature flag for factory pipelines along with the extra logging

### Fixes

*N/A*

## v 3.5.1

### Fixes

- Add default permissions needed for CodeBuild

## v 3.5.0

- Removed CodeStar Notification Rule for CodePipeline events
- Added CloudWatch Event Rule for CodePipeline events
- Added input transformer to format the event data
- Consolidate SNS Topics for events

### Fixes

*N/A*

## v 3.4.6

### Fixes

- Force detach policies from CodeBuild service role before deletion

## v 3.4.5

### Fixes

- Added the use for privilaged mode on the code_build project 

## v 3.4.4

### Fixes

- Crops the aws_iam_role, aws_codestarnotifications_notification_rule, and aws_cloudwatch_event_rule names, to ensure the 64 character length constraint is no longer an issue.

## v 3.4.3

### Fixes

- Crops the pipeline name used to make up the S3 Bucket name, to ensure the 63 character length constraint of an S3 Bucket name is no longer an issue.

## v 3.4.2
 **This change will replace your existing S3 Bucket for storing CodePipeline artifacts**
 - Added suffix to the S3 Bucket name for storing CodePipeline artifacts, to prevent conflicts

### Fixes

*N/A*

## v 3.4.1
- Removed AWS Config from the CodeBuildDeny IAM Boundary

### Fixes

*N/A*

## v 3.4.0
- Added custom_tags optional variable, and assigned to each resource

### Fixes

*N/A*

## v 3.3.2
- Enabled force_delete for S3 Bucket

### Fixes

- The removal of a pipeline would fail due to the S3 Bucket not being empty, adding the force_delete flag to the S3 Bucket resource allows for it to be deleted successfully

## v 3.3.1
- Renamed SNS Topic

### Fixes

- Conflicts when recreating a new resource with the same name, fixed by renaming the SNS Topic

## v 3.3.0
- Moved CodeBuild failure notifications to CloudWatch events

### Fixes

*N/A*

## v 3.2.4
- Added GuardDuty to IAM Boundary restricted access for CodeBuild role

### Fixes

*N/A*

## v 3.2.3
- Added a prefix to the CodeBuild name to help prevent conflicts

### Fixes

*N/A*

## v 3.2.2

*N/A*

### Fixes

- Malformed policy Sid

## v 3.2.1
- Prefix the CodeBuild service role permission boundary

### Fixes

- The prefix allows for multiple CodePipeline module definitions

## v 3.2.0
- Added a permissions boundary to the CodeBuild service role

### Fixes

*N/A*

## v 3.1.0
- Set up the notifications for the pipeline to an sns topic

### Fixes

*N/A*

## v 3.0.0
- Revert to using a single source per pipeline
- Remove the need to specify input and output artifacts

### Fixes

- Utilises the correct state 

## v 2.1.0
- Handle multiple sources and their artifacts, remove the need to pass input artifacts to the module.

### Fixes

*N/A*

## v 2.0.2
- updated the name of the iam roles to allow for multiple module calls

### Fixes

*N/A*

## v 2.0.1

### Features

- Fixed the loop for multiple Source repositories

### Fixes

*N/A*

## v 2.0.0

### Features

- Added the required ```codebuild_policy``` variable for passing in a CodeBuild resource policy, which will be utilised by CodeBuild in the Build steps of the CodePipeline.

### Fixes

*N/A*

## v 1.0.0

### Features

- Added support for multiple Codepipeline sources, this is a breaking change as the ```bitbucket_repository_name``` variable becomes ```bitbucket_repository_names``` and the data type changes from ```string``` to ```list```.

### Fixes

*N/A*

## v 0.1.1

### Features

*N/A*

### Fixes

- `depends_on` for `aws_iam_role_policy.codepipeline_policy` added to the CodePipeline resource. This prevents the source stage from making a connection via CodeStar to the source repository, until the CodePipeline service role has the necessary policy attached.

## v 0.1.0

### Features

- Initial Commit

### Fixes

*N/A*