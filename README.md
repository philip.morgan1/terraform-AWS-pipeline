AWS CodePipeline
==========

This module will provision an AWS [CodePipeline](https://docs.aws.amazon.com/codepipeline/index.html) and the stages required to run your repositories through the pipeline.

Usage
---

```js
module "codepipeline" {
  source                     = "../localmodules/terraform-AWS-pipeline"
  pipeline_name              = "codepipeline-terraform"
  bitbucket_workspace        = "ao-world-plc-open"
  bitbucket_repository_name  = "terraform-AWS-pipeline"
  codestar_connection_guid   = "00000000-0000-0000-0000-000000000000"
  codebuild_policy           = [file("./policy.json")]
  pipeline_stages            = file("./stages.json")
}
```

Required Variables
---

- `pipeline_name` : The name of the pipeline you wish to create

- `bitbucket_workspace` : The name of the Bitbucket workspace in which the repository lives

- `bitbucket_repository_name` : The name of the repository which will be the trigger source for the pipeline

- `codestar_connection_guid` : The CodeStar GUID for the connection to Bitbucket

- `codebuild_policy` : A collection of resource policies defined as JSON, which will be attached to the role used by your CodeBuild steps; please adhere to the principle of least privilege and only give CodeBuild the permissions required. **If your policy exceeds 10,240 characters it will need splitting into multiple policies, inline with the restrictions imposed by AWS**

- `pipeline_stages` : The CodePipeline stages declared as JSON.

  - `stageName` : The name for the CodePipeline stage being declared

  - `actionType` : The stage type, in the below snippet we declare a CodeBuild stage, which is the actionType `Build`

  - `environmentVariables` : An array of dictory objects containing the `name` and `value` of any variables you want to be available to that specific CodeBuild stage

**Example:**

```js
{
  "stages": [
    {
      "stageName": "qa",
      "actionType": "Build",
      "environmentVariables": [
        {
          "name": "env",
          "value": "qa"
        }
      ]
    }
  ]
}
```

Optional Variables
---

- `bitbucket_repository_branch_name` : The branch name of the source used to trigger the CodePipeline, defaults to `master`

- `build_image` : The build image used for your codebuild steps, defaults to `aws/codebuild/standard:4.0`

- `container_type` : The type of build environment available values are: `LINUX_CONTAINER`, `LINUX_GPU_CONTAINER`, `WINDOWS_CONTAINER` or `ARM_CONTAINER`; defaults to `LINUX_CONTAINER`

- `pipeline_stages`
    - `artifacts` : Specify any required input or output artifacts, as an array of strings, for CodePipeline stages. The original artifacts pulled from Bitbucket are named `source`

    - `buildspec` : The name of the buildspec file to be run in `CodeBuild`, defaults to `buildspec.yml`

    - `description` : A short description for CodeBuild projects
    
    - `build_timeout` : How long in minutes, from 5 to 480 (8 hours), for AWS CodeBuild to wait until timing out any related build that does not get marked as completed. The default is 60 minutes.
    
    - `queue_timeout` : How long in minutes, from 5 to 480 (8 hours), a build is allowed to be queued before it times out. The default is 8 hours.

- `custom_tags` : A map of tags which will be assigned to each resource created by the module.

Associated Costs
---

- `CodePipeline` : $1 per month for each active pipeline; the first 30 days of an active pipeline are free and unused pipelines have no associated costs. You can have one active pipeline per month within the free tier.

- `CodeBuild` : Pay as you go compute, charged per minute. The costs per type can be found [here](https://aws.amazon.com/codebuild/pricing/). 

- You may also be charged for data transfer and for the use of other AWS services in conjunction with CodePipeline and CodeBuild.
